﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BarcodeCreate
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
          

            bool createNew;
            using (new Mutex(true, Application.ProductName, out createNew))
            {
                if (createNew)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    try
                    {

                      // Application.Run(new CreateStudentBarCode());
                        Application.Run(new CreateStudentBarCode());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Info");
                        Application.Exit();
                    }
                }
                else
                {
                    MessageBox.Show(@"程序已经运行!", @"信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
