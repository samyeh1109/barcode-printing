﻿using AxAcroPDFLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BarcodeCreate
{
    public partial class FrmBatchPrint : Form
    {
        public FrmBatchPrint()
        {
            InitializeComponent();
            this.txtPath.ReadOnly = true;
        }

        private void btnBtachPrint_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtPath.Text.Trim()))
            {
                MessageBox.Show("请选择目录", "信息提示");
                return;
            }

            List<string> fileList = getPath(txtPath.Text.Trim());
            foreach(string filename in fileList)
            {
                if (File.Exists(filename))
                {
                    AxAcroPDF ac = new AxAcroPDF();
                    ((System.ComponentModel.ISupportInitialize)(ac)).BeginInit();
                    this.Controls.Add(ac);
                    ((System.ComponentModel.ISupportInitialize)(ac)).EndInit();
                    ac.LoadFile(filename);
                    ac.printAll();
                }
            }
            MessageBox.Show("批量打印成功!", "信息提示");

        }

        public static List<string> getPath(string path)
        {
            List<string> list = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] fil = dir.GetFiles();
            DirectoryInfo[] dii = dir.GetDirectories();
            foreach (FileInfo f in fil)
            {
                string ext = Path.GetExtension(f.FullName);
               if (ext.ToLower().Equals(".pdf"))
               {
                   list.Add(f.FullName);//添加文件的路径到列表
               }
            }
            //获取子文件夹内的文件列表，递归遍历
            foreach (DirectoryInfo d in dii)
            {
                getPath(d.FullName);
                string ext = Path.GetExtension(d.FullName);
                if (ext.ToLower().Equals(".pdf"))
                {
                    list.Add(d.FullName);//添加文件夹的路径到列表
                }
            }
            return list;
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            txtPath.Text = path.SelectedPath;
        }
    }
}
