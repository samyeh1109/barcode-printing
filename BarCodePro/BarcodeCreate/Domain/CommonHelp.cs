﻿using AxAcroPDFLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;

namespace BarcodeCreate
{
    public class CommonHelp
    {
        public static string GetCurrentRootRoute()
        {
            string dataDir = AppDomain.CurrentDomain.BaseDirectory;
            if (dataDir.EndsWith(@"\bin\Debug\") || dataDir.EndsWith(@"\bin\Release\"))
            {
                dataDir = System.IO.Directory.GetParent(dataDir).Parent.Parent.FullName;
                AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
            }
            return dataDir;
        }


        public static List<string> PrintBarCode(string studentIdArray, string schoolName, string className, Dictionary<string, string> dicStuName, Dictionary<string, string> dicStuNum, string classId)
        {
           
            List<string> printFile = new List<string>();
            string sidArray = studentIdArray.Trim(',');
            var sidList = sidArray.Split(',').Where(x =>
                 x.Split(new char[] { ',' }).Count() > 0);

            int total = 0;
            int totalPages = 0;
            total = sidList.Count();
            totalPages = total % 44 == 0 ? total / 44 : total / 44 + 1;

            Dictionary<int, List<string>> pageInfo = new Dictionary<int, List<string>>();
            List<string> imgList = new List<string>();

            foreach (string sid in sidList)
            {
                // 生成条形码
                CreateBarCode(sid);
            }

            string tempCode = CommonHelp.GetCurrentRootRoute() + @"\Template\code.png";
            var fs1 = new FileStream(tempCode, FileMode.Open, FileAccess.Read);
            var br1 = new BinaryReader(fs1);
            byte[] imgbyte1 = br1.ReadBytes(Convert.ToInt32(fs1.Length));
            var ms1 = new MemoryStream(imgbyte1);
            imgbyte1 = null;
            fs1.Close();
            br1.Close();
            // 生成具有学校信息的条形码
            foreach (string sid in sidList)
            {
                string filename = Application.StartupPath + @"\temp\" + sid + "00000.png";
                if (File.Exists(filename))
                {
                    imgList.Add(filename);
                    Bitmap _bitmap1 = new Bitmap(System.Drawing.Image.FromStream(ms1));
                    Graphics g = Graphics.FromImage(_bitmap1);
                    Brush bush = new SolidBrush(Color.Black);
                    Pen p = new Pen(Color.Red);
                    System.Drawing.Rectangle rang = new System.Drawing.Rectangle(123, 115, 44, 20);
                    string stname = "";
                    string stNum = "";
                    dicStuName.TryGetValue(sid, out stname);
                    dicStuNum.TryGetValue(sid, out stNum);
                    FontFamily myFontFamily = new FontFamily("幼圆");
                    System.Drawing.Font myFont = new System.Drawing.Font(myFontFamily, 13, FontStyle.Bold);
                    g.DrawString(stname, myFont, bush, 10, 10);
                    g.DrawString(className, myFont, bush, 10, 35);
                    g.DrawString(stNum, myFont, bush, 210, 10);
                    int len = schoolName.Length;
                    if (len >= 9)
                    {
                        g.DrawString(schoolName, myFont, bush, 180, 35);
                    }
                    else
                    {
                        g.DrawString(schoolName, myFont, bush, 210, 35);
                    }

                    var fsReadCode = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    var brCode = new BinaryReader(fsReadCode);
                    byte[] imgByCode = brCode.ReadBytes(Convert.ToInt32(fsReadCode.Length));
                    var msCode = new MemoryStream(imgByCode);
                    imgByCode = null;
                    fsReadCode.Close();
                    brCode.Dispose();

                    System.Drawing.Image img = System.Drawing.Image.FromStream(msCode);
                    g.DrawImage(img, 0, 60);
                    g.Dispose();
                    _bitmap1.Save(filename);
                    _bitmap1.Dispose();

                }
            }


            if (totalPages > 1)
            {
                List<string> newImgList = new List<string>();
                Dictionary<int, List<string>> dicPage = CreatePageInfo(imgList, totalPages - 1);
                int curPage = 1;
                foreach (KeyValuePair<int, List<string>> keyinfo in dicPage)
                {
                    printFile.Add(PdfHelper.CreateDetailFile(keyinfo.Value, curPage));
                    curPage++;
                    for (int i = 0; i < keyinfo.Value.Count; i++)
                    {
                        imgList.Remove(keyinfo.Value[i]);
                    }
                
                }


                printFile.Add(PdfHelper.CreateDetailFile(imgList, curPage));
            }
            else
            {
                printFile.Add(PdfHelper.CreateDetailFile(imgList, 1));
            }
            return printFile;

        }



        public static List<string> PrintBarCode(string studentIdArray, string schoolName, string className, Dictionary<string, string> dicStuName, Dictionary<string, string> dicStuNum, string classId, int number)
        {

            List<string> printFile = new List<string>();
            string sidArray = studentIdArray.Trim(',');
            var sidList = sidArray.Split(',').Where(x =>
                 x.Split(new char[] { ',' }).Count() > 0);

            Dictionary<int, List<string>> pageInfo = new Dictionary<int, List<string>>();
            List<string> imgList = new List<string>();

            foreach (string sid in sidList)
            {
                // 生成条形码
                CreateBarCode(sid);
            }

            string tempCode = CommonHelp.GetCurrentRootRoute() + @"\Template\code.png";
            var fs1 = new FileStream(tempCode, FileMode.Open, FileAccess.Read);
            var br1 = new BinaryReader(fs1);
            byte[] imgbyte1 = br1.ReadBytes(Convert.ToInt32(fs1.Length));
            var ms1 = new MemoryStream(imgbyte1);
            imgbyte1 = null;
            fs1.Close();
            br1.Close();
            // 生成具有学校信息的条形码
            foreach (string sid in sidList)
            {
                string filename = Application.StartupPath + @"\temp\" + sid + "00000.png";
                if (File.Exists(filename))
                {
                    imgList.Add(filename);
                    Bitmap _bitmap1 = new Bitmap(System.Drawing.Image.FromStream(ms1));
                    Graphics g = Graphics.FromImage(_bitmap1);
                    Brush bush = new SolidBrush(Color.Black);
                    Pen p = new Pen(Color.Red);
                    System.Drawing.Rectangle rang = new System.Drawing.Rectangle(123, 115, 44, 20);
                    string stname = "";
                    string stNum = "";
                    dicStuName.TryGetValue(sid, out stname);
                    dicStuNum.TryGetValue(sid, out stNum);
                    FontFamily myFontFamily = new FontFamily("幼圆");
                    Font myFont =new Font(myFontFamily, 13, FontStyle.Bold);
                    g.DrawString(stname, myFont, bush, 10, 10);
                    g.DrawString(className, myFont, bush, 10, 35);
                    g.DrawString(stNum, myFont, bush, 210, 10);
                    g.DrawString(schoolName, myFont, bush, 210, 35);

                    var fsReadCode = new FileStream(filename, FileMode.Open, FileAccess.Read);
                    var brCode = new BinaryReader(fsReadCode);
                    byte[] imgByCode = brCode.ReadBytes(Convert.ToInt32(fsReadCode.Length));
                    var msCode = new MemoryStream(imgByCode);
                    imgByCode = null;
                    fsReadCode.Close();
                    brCode.Dispose();

                    System.Drawing.Image img = System.Drawing.Image.FromStream(msCode);
                    g.DrawImage(img, 0, 60);
                    g.Dispose();
                    _bitmap1.Save(filename);
                    _bitmap1.Dispose();

                }
            }

            List<string> newFilePath = new List<string>(); // 新的图片集合
            for (int p=0; p<imgList.Count; p++)
            {
                // 每个人设置多少份
                for (int i = 0; i < number; i++)
                {
                    newFilePath.Add(imgList[p].ToString());
                }
            }

            int total = 0;
            int totalPages = 0;
            total = newFilePath.Count();
            totalPages = total % 44 == 0 ? total / 44 : total / 44 + 1;

            if (totalPages > 1)
            {
                List<string> newImgList = new List<string>();
                Dictionary<int, List<string>> dicPage = CreatePageInfo(newFilePath, totalPages - 1);
                int curPage = 1;
                foreach (KeyValuePair<int, List<string>> keyinfo in dicPage)
                {
                    printFile.Add(PdfHelper.CreateDetailFile(keyinfo.Value, curPage));
                    curPage++;
                    for (int i = 0; i < keyinfo.Value.Count; i++)
                    {
                        newFilePath.Remove(keyinfo.Value[i]);
                    }

                }

                printFile.Add(PdfHelper.CreateDetailFile(newFilePath, curPage));
            }
            else
            {
                printFile.Add(PdfHelper.CreateDetailFile(newFilePath, 1));
            }
            return printFile;

        }

     
        private static Dictionary<int, List<string>> CreatePageInfo(List<string> imgList, int page)
        {
            Dictionary<int, List<string>> dicPage = new Dictionary<int, List<string>>();
            int Counter = 1;
               List<string> newImg = new List<string>();
            for (int j = 0; j < imgList.Count; j++)
            {
                if (Counter > page)
                    break;
                 newImg.Add(imgList[j]);
                if(j==(Counter*44-1))
                {
                         List<string> newAddList = Clone<List<string>>(newImg);
                         dicPage.Add(Counter, newAddList);
                         newImg.Clear();
                         Counter++;
                }
                
            }
            return dicPage;

           
        }


      


        /// <summary>
        /// 生成条形码
        /// </summary>
        /// <param name="barCode"></param>
        private static void CreateBarCode(string barCode)
        {

            if (!string.IsNullOrEmpty(barCode))
            {
                // 1.设置条形码规格
                EncodingOptions encodeOption = new EncodingOptions();
                encodeOption.Height = 123; // 必须制定高度、宽度
                encodeOption.Width = 397;
                encodeOption.PureBarcode = true;


                // 2.生成条形码图片并保存
                ZXing.BarcodeWriter wr = new BarcodeWriter();
                wr.Options = encodeOption;
                wr.Format = BarcodeFormat.CODE_128; //  条形码规格
                string autocode = barCode + "00000";
                Bitmap img = wr.Write(autocode);
                string path = Application.StartupPath + @"\temp";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string filePath = path + @"\" + autocode + ".png";
                img.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
                img.Dispose();

            }

        }

        public static T Clone<T>(T RealObject)
        {
            using (Stream objectStream = new MemoryStream())
            {
                //利用 System.Runtime.Serialization序列化与反序列化完成引用对象的复制
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(objectStream, RealObject);
                objectStream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(objectStream);
            }
        }

    }

    static class Extensions
    {
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
    }
}
