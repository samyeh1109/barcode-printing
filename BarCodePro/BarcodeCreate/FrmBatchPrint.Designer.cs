﻿namespace BarcodeCreate
{
    partial class FrmBatchPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBtachPrint = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnCheck = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnBtachPrint
            // 
            this.btnBtachPrint.Location = new System.Drawing.Point(64, 115);
            this.btnBtachPrint.Name = "btnBtachPrint";
            this.btnBtachPrint.Size = new System.Drawing.Size(75, 23);
            this.btnBtachPrint.TabIndex = 0;
            this.btnBtachPrint.Text = "批量打印";
            this.btnBtachPrint.UseVisualStyleBackColor = true;
            this.btnBtachPrint.Click += new System.EventHandler(this.btnBtachPrint_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(64, 44);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(75, 23);
            this.btnCheck.TabIndex = 1;
            this.btnCheck.Text = "选择路径";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(145, 46);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(188, 19);
            this.txtPath.TabIndex = 2;
            // 
            // FrmBatchPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 245);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.btnBtachPrint);
            this.Name = "FrmBatchPrint";
            this.Text = "FrmBatchPrint";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBtachPrint;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.TextBox txtPath;
    }
}