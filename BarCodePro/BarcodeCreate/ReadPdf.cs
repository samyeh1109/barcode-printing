﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BarcodeCreate
{
    public partial class ReadPdf : Form
    {
        public ReadPdf()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string temp = GetCurrentRootRoute() + @"\Template\barcodeTemp.pdf";
            string newfile = GetCurrentRootRoute() + @"\Template\barcodeTempnew.pdf";
            string barcode = GetCurrentRootRoute() + @"\Template\codetest.png";
            Dictionary<string, string> dir = new Dictionary<string, string>();
            GeneratorPdf(temp, newfile,dir, barcode);
        }

        public  string GetCurrentRootRoute()
        {
            string dataDir = AppDomain.CurrentDomain.BaseDirectory;
            if (dataDir.EndsWith(@"\bin\Debug\") || dataDir.EndsWith(@"\bin\Release\"))
            {
                dataDir = System.IO.Directory.GetParent(dataDir).Parent.Parent.FullName;
                AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
            }
            return dataDir;
        }


        public static void GeneratorPdf(String templatePath, String targetPath, Dictionary<string, string> parameters, String imagePath)
        {

            // 读取模板文件
            PdfReader pdfReader = new PdfReader(templatePath);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(targetPath, FileMode.Create));
            // 提取pdf中的表单
            AcroFields form = pdfStamper.AcroFields;
            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            form.AddSubstitutionFont(bf);
          
                // 通过域名获取所在页和坐标，左下角为起点

                int pageNo = form.GetFieldPositions("img1").First().page;
               IList<AcroFields.FieldPosition> list= form.GetFieldPositions("img1");

               foreach (AcroFields.FieldPosition info in list)
               {
                   iTextSharp.text.Rectangle signRect = info.position;
                   float x = signRect.GetLeft(0);
                   float y = signRect.GetBottom(10);

                   // 读图片
                   iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);
                   // 获取操作的页面
                   PdfContentByte under = pdfStamper.GetOverContent(pageNo);
                   // 根据域的大小缩放图片
                   image.ScaleToFit(signRect);
                   // 添加图片
                   image.SetAbsolutePosition(x, y);
                   under.AddImage(image);

               }
                //iTextSharp.text.Rectangle signRect = form.GetFieldPositions("img1").First().position;
                //float x = signRect.GetLeft(0);
                //float y = signRect.GetBottom(0);

                //// 读图片
                //iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagePath);
                //// 获取操作的页面
                //PdfContentByte under = pdfStamper.GetOverContent(pageNo);
                //// 根据域的大小缩放图片
                //image.ScaleToFit(signRect);
                //// 添加图片
                //image.SetAbsolutePosition(x, y);
                //under.AddImage(image);
            

            // 添加文字
                if (parameters.Count > 0)
                {
                    foreach (KeyValuePair<string, string> parameter in parameters)
                    {
                        form.SetField(parameter.Key, parameter.Value);

                    }
                }

            pdfStamper.FormFlattening = true;
            pdfStamper.Close();
            pdfReader.Close();
        }
    }
}
