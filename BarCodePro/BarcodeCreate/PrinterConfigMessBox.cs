﻿using AxAcroPDFLib;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace BarcodeCreate
{
    public partial class PrinterConfigMessBox : Form
    {

        public List<string>  FileList { get; set; }
    
        public PrinterConfigMessBox()
        {
            InitializeComponent();
           // this.Load += new EventHandler(PrinterConfigMessBox_Load);
      

        }

        public PrinterConfigMessBox(List<string> FileList):this()
        {
        
            this.FileList = FileList;

        }



        public void PrinterConfigMessBox_Load()  // object sender, EventArgs e
        {
            if (FileList == null)
                return;
            if (FileList.Count == 0)
                return;

            for (int i = 0; i < FileList.Count; i++)
            {
                AxAcroPDF ac = new AxAcroPDF();
                ((System.ComponentModel.ISupportInitialize)(ac)).BeginInit();
                 this.Controls.Add(ac);
               // this.tabControl1.TabPages[i].Controls.Add(ac);
                ((System.ComponentModel.ISupportInitialize)(ac)).EndInit();
                ac.LoadFile(FileList[i].ToString());
                ac.Dock = DockStyle.Fill;

                if (i >= 1)
                {
                    TabPage tabpage = new TabPage();
                    tabpage.Text = "第" + (i + 1).ToString() + "页";
                    this.tabControl1.TabPages.Insert(i, tabpage);
                    this.tabControl1.TabPages[i].Show();
                    this.tabControl1.TabPages[i].Controls.Clear();
                    this.tabControl1.TabPages[i].Controls.Add(ac);
                }
                else
                {
                    this.tabControl1.TabPages[i].Show();
                
                    this.tabControl1.TabPages[i].Controls.Add(ac);
                }
            }
            
        }

   

 

       

        private void printtest()
        {
             string fileName = Application.StartupPath + @"\temp\201801111255581926.pdf";
        }

      
        /// <summary>
        /// 批量打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            this.btnPrint.Enabled = false;
          
            if (this.FileList != null && this.FileList.Count > 0)
            {
                for (int i = 0; i < FileList.Count; i++)
                {
                    AxAcroPDF ac = new AxAcroPDF();
                    ((System.ComponentModel.ISupportInitialize)(ac)).BeginInit();
                    this.Controls.Add(ac);
                    ((System.ComponentModel.ISupportInitialize)(ac)).EndInit();
                    ac.LoadFile(FileList[i].ToString());
                    ac.printAll();

                }
            }
            this.btnPrint.Enabled = true;
            MessageBox.Show("打印成功!", "信息提示");
        }

        /// <summary>
        /// 批量保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {

           
            int curPage = 1;
            string newfilename = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + curPage.ToString() + ".pdf";
            if (this.FileList != null && this.FileList.Count > 0)
            {
                if (this.FileList.Count == 1)
                {
                    newfilename = this.FileList[0].ToString();
                }
                else
                {
                    PdfHelper.MergePDFFiles(this.FileList, newfilename);
                }

                string localFilePath = String.Empty;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                //设置文件类型  
                saveFileDialog1.Filter = "PDF文件|*.pdf";
                //设置文件名称：
                saveFileDialog1.FileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_news.pdf";
                curPage++;

                //设置默认文件类型显示顺序  
                saveFileDialog1.FilterIndex = 2;

                //保存对话框是否记忆上次打开的目录  
                saveFileDialog1.RestoreDirectory = true;

                //点了保存按钮进入  
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    File.Copy(newfilename, saveFileDialog1.FileName, true);
                    MessageBox.Show("保存成功!", "信息提示");
                }

                //foreach (string filename in FileList)
                //{
                //    string localFilePath = String.Empty;
                //    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                //    //设置文件类型  
                //    saveFileDialog1.Filter = "PDF文件|*.pdf";
                //    //设置文件名称：
                //    saveFileDialog1.FileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_" + curPage.ToString() + ".pdf";
                //    curPage++;

                //    //设置默认文件类型显示顺序  
                //    saveFileDialog1.FilterIndex = 2;

                //    //保存对话框是否记忆上次打开的目录  
                //    saveFileDialog1.RestoreDirectory = true;

                //    //点了保存按钮进入  
                //    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                //    {
                //        File.Copy(filename, saveFileDialog1.FileName, true);
                //        MessageBox.Show("保存成功!", "信息提示");


                //    }
                //}
            }
           
        }

       
    }
}
