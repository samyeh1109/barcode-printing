﻿using AxAcroPDFLib;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BarcodeCreate
{
    public partial class CreateStudentBarCode : Form
    {
        public static Dictionary<string, string> dicStuNum = new Dictionary<string, string>();
        public static Dictionary<string, string> dicStuName = new Dictionary<string, string>();
       private List<string> GloableList;
       private string currentSchoolName = "";
       private string currentCLassName = "";
        public CreateStudentBarCode()
        {
            InitializeComponent();
         //   MySqlHelper.connstr =ConfigHelper.GetAppSetting("DBConnection");
            MySqlHelper.connstr = EnvironFrm.GetDataConnectionString();
            // 防止线程调用窗体是发生争用等错误
            CheckForIllegalCrossThreadCalls = false;
            // this.cboClass.DrawMode = DrawMode.OwnerDrawVariable;
            GloableList = new List<string>();
            this.cboStudent.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cboClass.DropDownStyle = ComboBoxStyle.DropDownList;
            cboStudent.ItemHeight = 22;
            this.printDocument1.OriginAtMargins = true;//启用页边距
            this.pageSetupDialog1.EnableMetric = true; //以毫米为单位
            this.txtUserName.MaxLength = 160;
            this.KeyPreview = true;
            this.txtUserName.KeyUp += delegate(object sender, KeyEventArgs e) 
            {
                if (e.KeyCode == Keys.Control || e.KeyCode == Keys.Enter)
                {
                    btnSeach_Click(sender, e);
                }  
            };
            this.KeyDown += delegate(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSeach_Click(sender, e);
                }
            };
            this.cboClass.TextChanged += delegate 
            {

               
            };
        
            // cboStudent.DrawMode = DrawMode.OwnerDrawFixed;
            // cboStudent.ValueMember = "SCH_UUID";
            // cboStudent.DisplayMember = "SCH_NAME";
            // cboStudent.DrawItem += new DrawItemEventHandler(delegate(object sender, DrawItemEventArgs e)
            // {
            //     if (e.Index < 0)
            //     {
            //         return;
            //     }
            //     e.DrawBackground();
            //     e.DrawFocusRectangle();
            //     e.Graphics.DrawString(cboStudent.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds.X, e.Bounds.Y + 3);
            // });

            //cboClass.DropDownStyle = ComboBoxStyle.DropDownList;
            //cboClass.ItemHeight = 22;
            //cboClass.DrawMode = DrawMode.OwnerDrawFixed;

            //cboClass.DrawItem += new DrawItemEventHandler(delegate(object sender, DrawItemEventArgs e)
            //{
            //    if (e.Index < 0)
            //    {
            //        return;
            //    }
            //    e.DrawBackground();
            //    e.DrawFocusRectangle();
            //    e.Graphics.DrawString(cboClass.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds.X, e.Bounds.Y + 3);
            //});
            //this.cboClass.DrawItem += delegate(object sender, DrawItemEventArgs e)
            //{
            //    if (e.Index < 0)
            //    {
            //        return;
            //    }
            //    e.DrawBackground();
            //    e.DrawFocusRectangle();
            //    e.Graphics.DrawString(cboClass.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds.X, e.Bounds.Y + 3);
            //};
        }


        private void CreateStudentBarCode_Load(object sender, EventArgs e)
        {
            lblTotal.Text = "";
            skinEngine1.SkinFile = Application.StartupPath + @"\MP10.ssk";
            string fileDir = Application.StartupPath + @"\temp";
            if (!Directory.Exists(fileDir))
                Directory.CreateDirectory(fileDir);
            DirectoryInfo dir = new DirectoryInfo(fileDir);
            FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
            foreach (FileSystemInfo i in fileinfo)
            {
                if (i is DirectoryInfo)            //判断是否文件夹
                {
                    DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                    subdir.Delete(true);          //删除子目录和文件
                }
                else
                {
                    File.Delete(i.FullName);      //删除指定文件
                }
            }           

            string strSql = @"select SCH_UUID,SCH_NAME from t_school where DEL_FLG=0  ORDER BY UPDATE_TIME DESC";
            DataTable dt = MySqlHelper.ExecuteDataTable(strSql);

            DataRow newRow;
            newRow = dt.NewRow();
            newRow["SCH_UUID"] = "";
            newRow["SCH_NAME"] = "--请选择--";

            dt.Rows.InsertAt(newRow, 0);
            cboStudent.ValueMember = "SCH_UUID";
            cboStudent.DisplayMember = "SCH_NAME";
            cboStudent.DataSource = dt;

            DataTable dtClass = new DataTable();
            DataColumn dc = dtClass.Columns.Add("CLASS_UUID", Type.GetType("System.String"));
            DataColumn dc2 = dtClass.Columns.Add("CLASS_NAME", Type.GetType("System.String"));
            if (!dtClass.Columns.Contains("CLASS_UUID"))
            {
                dtClass.Columns.Add(dc);
            }
            if (!dtClass.Columns.Contains("CLASS_NAME"))
            {
                dtClass.Columns.Add(dc2);
            }
            DataRow newRowCalss;
            newRowCalss = dtClass.NewRow();
            newRowCalss["CLASS_UUID"] = "";
            newRowCalss["CLASS_NAME"] = "--请选择--";
            dtClass.Rows.Add(newRowCalss);


            this.cboClass.ValueMember = "CLASS_UUID";
            this.cboClass.DisplayMember = "CLASS_NAME";
            this.cboClass.DataSource = dtClass;

            this.cboClass.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.cboClass.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
        }


     
        private void cboStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            int isCheck = cboStudent.SelectedIndex;

            if (isCheck > 0)
            {
                string strSqlClass = string.Format(@"select CLASS_UUID,CLASS_NAME from t_classes  WHERE SCH_UUID='{0}'  and DEL_FLG=0  ORDER BY CLASS_CODE ASC", cboStudent.SelectedValue.ToString());
                DataTable dt = MySqlHelper.ExecuteDataTable(strSqlClass);
                DataRow newRow;
                newRow = dt.NewRow();
                newRow["CLASS_UUID"] = "";
                newRow["CLASS_NAME"] = "--请选择--";
              
                dt.Rows.InsertAt(newRow, 0);
                cboClass.ValueMember = "CLASS_UUID";
                cboClass.DisplayMember = "CLASS_NAME";
                cboClass.DataSource = dt;
            }
            else
            {
                DataTable dtClass = new DataTable();
                DataColumn dc = null;
                dc = dtClass.Columns.Add("CLASS_UUID", Type.GetType("System.String"));
                dc = dtClass.Columns.Add("CLASS_NAME", Type.GetType("System.String"));
                DataRow newRowCalss;
                newRowCalss = dtClass.NewRow();
                newRowCalss["CLASS_UUID"] = "";
                newRowCalss["CLASS_NAME"] = "--请选择--";
                dtClass.Rows.Add(newRowCalss);

                this.cboClass.ValueMember = "CLASS_UUID";
                this.cboClass.DisplayMember = "CLASS_NAME";
                this.cboClass.DataSource = dtClass;
            }


        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSeach_Click(object sender, EventArgs e)
        {
            GloableList.Clear();
            ClearControl();
            dicStuNum.Clear();
            dicStuName.Clear();
            int stucid = this.cboStudent.SelectedIndex;
            int classid = this.cboClass.SelectedIndex;
            currentSchoolName = cboStudent.Text;
            currentCLassName = cboClass.Text;
            if (stucid <= 0)
            {
                MessageBox.Show("请选择学校!", "信息提示");
                return;
            }
            if (classid <= 0)
            {
                MessageBox.Show("请选择班级!", "信息提示");
                return;
            }
         
            DataTable dt = GetStuNameBySql();
            lblTotal.Text = "共 " + dt.Rows.Count.ToString() + " 个学生";
            if (dt != null && dt.Rows.Count > 0)
            {
                int interval = 230;
                int pint = 0;
                foreach (DataRow rows in dt.Rows)
                {
                    if (!dicStuNum.ContainsKey(rows["STU_UUID"].ToString()))
                    {
                        dicStuNum.Add(rows["STU_UUID"].ToString(), rows["STU_NUM"].ToString());
                    }
                    if (!dicStuName.ContainsKey(rows["STU_UUID"].ToString()))
                    {
                        dicStuName.Add(rows["STU_UUID"].ToString(), rows["STU_NAME"].ToString());
                    }

                    CheckBox cbk = new CheckBox();
                    cbk.Tag = rows["STU_UUID"].ToString();
                    cbk.Text = rows["STU_NAME"].ToString();
                    int xcoordinate = interval * pint + 86;
                    cbk.Location = new Point(xcoordinate, 43);
                    cbk.Size = new Size(150, 30);
                    cbk.CheckedChanged += delegate {
                        if (GloableList != null && GloableList.Count > 0)
                        {
                            GloableList.Clear();
                        }
                    };
                    flpStudentList.Controls.Add(cbk);
                    pint++;
                }
            }


        }

        private void ExecuteDataTable()
        {
           
        }

        private DataTable GetStuNameBySql()
        {

            MySqlParameter[] prams;
            StringBuilder sb = new StringBuilder();
            sb.Append("select  DISTINCT t1.STU_UUID,t2.STU_NAME,t2.STU_NUM from").AppendLine();
            sb.Append("t_stu_partake  t1 inner join t_student t2").AppendLine();
            sb.Append("on t1.STU_UUID=t2.STU_UUID").AppendLine();
            sb.Append("where t1.SCH_UUID=@SCH_UUID and t1.CLASS_UUID=@CLASS_UUID  and  t1.SIGN_FLG=1").AppendLine();
            if (!string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                prams = new MySqlParameter[3];
                sb.Append("and t2.STU_NAME like @STU_NAME ").AppendLine();
            }
            else
            {
                prams = new MySqlParameter[2];
             
            }

         //   sb.Append(" ORDER BY t1.CREATE_TIME desc");
            sb.Append(" ORDER BY t1.STU_UUID ASC");

            prams[0] = new MySqlParameter("@SCH_UUID", cboStudent.SelectedValue.ToString());
            prams[1] = new MySqlParameter("@CLASS_UUID", cboClass.SelectedValue.ToString());
            if (!string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {

                prams[2] = new MySqlParameter("@STU_NAME", "%" + txtUserName.Text.Trim() + "%");

            }
          

          DataTable dt=  MySqlHelper.ExecuteDataTable(sb.ToString(), prams);
          return dt;
        }

        private void ClearControl()
        {
            foreach (Control c in flpStudentList.Controls)
            {

                string s = c.Tag.ToString();
                flpStudentList.Controls.Remove(c);
                flpStudentList.Controls.Clear();
            }
        }

        /// <summary>
        /// 直接批量打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
              
                List<string> fileList = PrintFile(0);
                if (fileList.Count == 0)
                    return;
                this.btnPrint.Enabled = false;
                for (int i = 0; i < fileList.Count; i++)
                {
                    AxAcroPDF ac = new AxAcroPDF();
                    ((System.ComponentModel.ISupportInitialize)(ac)).BeginInit();
                    this.Controls.Add(ac);
                    ((System.ComponentModel.ISupportInitialize)(ac)).EndInit();
                    ac.LoadFile(fileList[i].ToString());
                    ac.printAll();

                }
                this.btnPrint.Enabled = true;
                MessageBox.Show("打印成功!", "信息提示");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "信息提示!");
            }

        }

        private List<string> PrintFile(int type = 0)
        {
            List<string> fileList = new List<string>();
          
            StringBuilder sb = new StringBuilder();
            foreach (Control c in flpStudentList.Controls)
            {
                if (c.GetType().Name.Equals("CheckBox"))
                {
                    CheckBox cbo = c as CheckBox;
                    if (cbo.Checked)
                    {
                        sb.Append(cbo.Tag).Append(",");
                     
                    }
                }
            }
       
            string idList = sb.ToString();
            if (String.IsNullOrEmpty(idList))
            {
                if (type == 0)
                    MessageBox.Show("没有选择可以打印的学生!", "信息提示");
                else
                    MessageBox.Show("没有可以预览的文件!", "信息提示");
                return fileList;
            }

            fileList = CommonHelp.PrintBarCode(idList,currentSchoolName, currentCLassName, dicStuName, dicStuNum, cboClass.SelectedValue.ToString());
           // fileList = CommonHelp.PrintBarCode(idList, cboStudent.Text, cboClass.Text, dicStuName, dicStuNum, cboClass.SelectedValue.ToString());
            return fileList;




        }

        /// <summary>
        /// 批量打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrintPer_Click(object sender, EventArgs e)
        {

            if (GloableList != null && GloableList.Count > 0)
            {
                PrinterConfigMessBox p = new PrinterConfigMessBox(GloableList);
                p.Show();
                p.PrinterConfigMessBox_Load();
            }
            else
            {
             
                List<string> fileList = PrintFile(1);
                GloableList = fileList;
                if (fileList.Count == 0)
                {
                    return;
                }
                //  List<string> fileListtext = new List<string>();
                // string filename1 = Application.StartupPath + @"\temp\201801120943486082.pdf";
                //  string filename2 = Application.StartupPath + @"\temp\201801120943499981.pdf";
                //  fileListtext.Add(filename1);
                //  fileListtext.Add(filename2);
                PrinterConfigMessBox p = new PrinterConfigMessBox(fileList);
                p.Show();
                p.PrinterConfigMessBox_Load();
            }
        }

        private void btnAllCheck_Click(object sender, EventArgs e)
        {
            foreach (Control c in flpStudentList.Controls)
            {
                if (c.GetType().Name.Equals("CheckBox"))
                {
                    CheckBox cbo = c as CheckBox;
                    cbo.Checked = true;
                }
            }
        }

        private void btnNoCheck_Click(object sender, EventArgs e)
        {
            foreach (Control c in flpStudentList.Controls)
            {
                if (c.GetType().Name.Equals("CheckBox"))
                {
                    CheckBox cbo = c as CheckBox;
                    if (cbo.Checked)
                    {
                        cbo.Checked = false;
                    }
                    else
                    {
                        cbo.Checked = true;
                    }
                }
            }
        }

        private void btnSignle_Click(object sender, EventArgs e)
        {


            List<string> fileList = new List<string>();
          
            StringBuilder sb = new StringBuilder();
            foreach (Control c in flpStudentList.Controls)
            {
                if (c.GetType().Name.Equals("CheckBox"))
                {
                    CheckBox cbo = c as CheckBox;
                    if (cbo.Checked)
                    {
                        sb.Append(cbo.Tag).Append(","); // Stuuuid
                      
                    }
                }
            }
            string idList = sb.ToString();
            if (String.IsNullOrEmpty(idList))
            {

                MessageBox.Show("没有选择可以打印的学生!", "信息提示");
                return;
            }
           // FrmSingle frm = new FrmSingle(cboStudent.Text, cboClass.Text,idList, dicStuNum, dicStuName);
            FrmSingle frm = new FrmSingle(currentSchoolName, currentCLassName, idList, dicStuNum, dicStuName);
            frm.Show();
        }

        private void btnSelectDs_Click(object sender, EventArgs e)
        {
            EnvironFrm frm = new EnvironFrm();
            frm.ShowDialog();
        }


    }
}


